import {LoginModel} from './../models/shared/user/login-model';
import {Constants} from '../models/shared/constants';
import {RegisterModel} from './../models/shared/user/register-model';
import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthorizationService {
  private authEndpoint = `${Constants.API_ENDPOINT}/auth`;

  constructor(
    private http: HttpClient,
    public router: Router
  ) {
  }

  signUp(user: RegisterModel) {
    const api = `${this.authEndpoint}/sign-up`;
    return this.http.post(api, user);
  }

  signIn(user: LoginModel) {
    return this.http.post<any>(`${this.authEndpoint}/sign-in`, user)
  }

  getToken() {
    return localStorage.getItem('access_token');
  }

  get isLoggedIn(): boolean {
    const authToken = localStorage.getItem('access_token');
    return authToken != null;
  }

  logout() {
    const removeToken = localStorage.removeItem('access_token');
    if (removeToken == null) {
      this.router.navigate(['log-in']);
    }
  }

  setTokenAndRedirectToHomePage(token: string, home: string) {
    localStorage.setItem('access_token', token);
    this.router.navigate([home]);
  }

  setToken(token: string) {
    localStorage.setItem('access_token', token);
  }
}
