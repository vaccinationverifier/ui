import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';
import {CreateHospitalRequest} from 'src/models/hospital/create-hospital-request';
import {HospitalResponse} from 'src/models/hospital/hospital-response';
import {Address} from 'src/models/shared/address';
import {Constants} from 'src/models/shared/constants';

@Injectable({
  providedIn: 'root'
})
export class HospitalService {
  private readonly hospitalsEndpointBase: string = `${Constants.API_ENDPOINT}/hospitals`;

  constructor(
    private httpClient: HttpClient
  ) {
  }

  create(request: CreateHospitalRequest): Observable<string> {
    return this.httpClient.post<string>(this.hospitalsEndpointBase, request);
  }

  getByAddress(address: Address): Observable<HospitalResponse[]> {
    // const path = this.fillQuery(this.hospitalsEndpointBase, address);
    //
    // return this.httpClient.get<HospitalResponse[]>(path);
    const hospitals: HospitalResponse[] = [
      {
        name: "Some name",
        id: "0EC0AE9E-A291-46B1-E7EE-08D8FF60B643",
        address: {
          city: "Kharkiv",
          street: "blablabla",
          country: " Ukraine"
        }
      }
    ];

    return of<HospitalResponse[]>(hospitals);
  }

  private fillQuery(basePath: string, obj: any) {
    const params = new URLSearchParams();
    for (let key in obj) {
      params.set(key, obj[key])
    }

    return `${basePath}?${params.toString()}`;
  }

  delete(id: string): Observable<any> {
    return this.httpClient.delete(`${this.hospitalsEndpointBase}/${id}`);
  }
}
