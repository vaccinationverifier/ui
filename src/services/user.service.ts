import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Constants } from 'src/models/shared/constants';
import { BanUserRequest } from 'src/models/shared/user/ban-user-request';
import { UserInfo } from 'src/models/shared/user/user-info';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private readonly usersEndpointBase: string = `${Constants.API_ENDPOINT}/users`;

  constructor(
    private httpClient: HttpClient
  ) { }

  me(): Observable<UserInfo> {
    return this.httpClient.get<UserInfo>(`${this.usersEndpointBase}/me`);
  }

  getAll(): Observable<UserInfo[]> {
    return this.httpClient.get<UserInfo[]>(this.usersEndpointBase);
  }

  getBanPeriods(): string[] {
    return ['Hour', 'Day', 'Month', 'Year', 'Forever', 'Custom'];
  }

  getCustomBanPeriod(): string {
    return 'Custom';
  }

  banUser(id: string, request: BanUserRequest): Observable<any> {
    return this.httpClient.post(`${this.usersEndpointBase}/${id}/ban`, request);
  }

  downloadBackup(): Observable<Blob> {
    const url = `${Constants.API_ENDPOINT}/admin/backup`;
    return this.httpClient.get(url, { responseType: 'blob' });
  }
}
