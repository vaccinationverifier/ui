import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CreateDoctorRequest } from 'src/models/doctor/create-doctor-request';
import { DoctorResponse } from 'src/models/doctor/doctor-response';
import { BlobResourceResponse } from 'src/models/shared/blob-resource-response';
import { Constants } from 'src/models/shared/constants';

@Injectable({
  providedIn: 'root'
})
export class DoctorService {
  private readonly doctorsEndpointBase: string = `${Constants.API_ENDPOINT}/doctors`;

  constructor(
    private httpClient: HttpClient
  ) { }

  create(request: CreateDoctorRequest): Observable<string> {
    return this.httpClient.post<string>(this.doctorsEndpointBase, request);
  }

  getAll(): Observable<DoctorResponse[]> {
    return this.httpClient.get<DoctorResponse[]>(this.doctorsEndpointBase);
  }

  getDoctorImage(id): Observable<BlobResourceResponse> {
    return this.httpClient.get<BlobResourceResponse>(`${this.doctorsEndpointBase}/${id}/image`)
  }

  verifyDoctor(id): Observable<any> {
    return this.httpClient.post(`${this.doctorsEndpointBase}/${id}/verify`, {});
  }
}
