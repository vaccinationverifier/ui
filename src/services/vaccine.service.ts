import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';
import {Constants} from 'src/models/shared/constants';
import {CreateVaccineRequest} from 'src/models/vaccine/create-vaccine-request';
import {VaccineResponse} from 'src/models/vaccine/vaccine-response';

@Injectable({
  providedIn: 'root'
})
export class VaccineService {
  private readonly vaccinesEndpointBase: string = `${Constants.API_ENDPOINT}/vaccines`;

  constructor(
    private httpClient: HttpClient
  ) {
  }

  create(request: CreateVaccineRequest): Observable<string> {
    return this.httpClient.post<string>(this.vaccinesEndpointBase, request);
  }

  getAll(): Observable<VaccineResponse[]> {
    // return of<VaccineResponse[]>([{
    //   id: "id",
    //   valence: "vallence",
    //   illnessName: "illnessName",
    //   description: "description",
    //   vaccineName: "vaccineName",
    //   license: "license",
    //   sideEffects: "sideEffects",
    //   type: "type",
    //   vendor: "vendor"
    // },
    //   {
    //     id: "id",
    //     valence: "vallence",
    //     illnessName: "illnessName",
    //     description: "description",
    //     vaccineName: "vaccineName",
    //     license: "license",
    //     sideEffects: "sideEffects",
    //     type: "type",
    //     vendor: "vendor"
    //   },
    //   {
    //     id: "id",
    //     valence: "vallence",
    //     illnessName: "illnessName",
    //     description: "description",
    //     vaccineName: "vaccineName",
    //     license: "license",
    //     sideEffects: "sideEffects",
    //     type: "type",
    //     vendor: "vendor"
    //   }, {
    //     id: "id",
    //     valence: "vallence",
    //     illnessName: "illnessName",
    //     description: "description",
    //     vaccineName: "vaccineName",
    //     license: "license",
    //     sideEffects: "sideEffects",
    //     type: "type",
    //     vendor: "vendor"
    //   }, {
    //     id: "id",
    //     valence: "vallence",
    //     illnessName: "illnessName",
    //     description: "description",
    //     vaccineName: "vaccineName",
    //     license: "license",
    //     sideEffects: "sideEffects",
    //     type: "type",
    //     vendor: "vendor"
    //   }, {
    //     id: "id",
    //     valence: "vallence",
    //     illnessName: "illnessName",
    //     description: "description",
    //     vaccineName: "vaccineName",
    //     license: "license",
    //     sideEffects: "sideEffects",
    //     type: "type",
    //     vendor: "vendor"
    //   }, {
    //     id: "id",
    //     valence: "vallence",
    //     illnessName: "illnessName",
    //     description: "description",
    //     vaccineName: "vaccineName",
    //     license: "license",
    //     sideEffects: "sideEffects",
    //     type: "type",
    //     vendor: "vendor"
    //   }]);
    return this.httpClient.get<VaccineResponse[]>(this.vaccinesEndpointBase);
  }

  delete(id: string) : Observable<any> {
    return this.httpClient.delete(`${this.vaccinesEndpointBase}/${id}`);
  }
}
