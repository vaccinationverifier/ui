import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class LocalizationService {
  constructor(private router: Router) { }

  getLocalizationPath(locale): string {
    const basePath = 'http://localhost';
    const locationToPortMap = {
      en: '4201',
      ua: '4200'
    };

    const currentPort = window.location.port;
    const targetPort = locationToPortMap[locale];

    if (currentPort === targetPort) {
      return this.router.url;
    }

    return `${basePath}:${targetPort}${this.router.url}`;
  }
}
