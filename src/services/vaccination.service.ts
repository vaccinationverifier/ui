import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import { BlobResourceResponse } from 'src/models/shared/blob-resource-response';
import {Constants} from 'src/models/shared/constants';
import {CreatedResponse} from 'src/models/shared/created-response';
import {CreateVaccinationRequest} from 'src/models/vaccination/create-vaccination-request';
import {VaccinationResponse} from 'src/models/vaccination/vaccination-response';

@Injectable({
  providedIn: 'root'
})
export class VaccinationService {
  private readonly vaccinationsEndpointBase: string = `${Constants.API_ENDPOINT}/vaccinations`;

  constructor(
    private httpClient: HttpClient,
  ) {
  }

  create(request: CreateVaccinationRequest): Observable<CreatedResponse> {
    return this.httpClient.post<CreatedResponse>(this.vaccinationsEndpointBase, request);
  }

  my(): Observable<VaccinationResponse[]> {
    return this.httpClient.get<VaccinationResponse[]>(`${this.vaccinationsEndpointBase}/my`);
  }

  getVaccinationVideo(id): Observable<BlobResourceResponse> {
    return this.httpClient.get<BlobResourceResponse>(`${this.vaccinationsEndpointBase}/${id}/video`);
  }
}
