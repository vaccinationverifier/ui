import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CreatePatientRequest } from 'src/models/patient/create-patient-request';
import { PatientResponse } from 'src/models/patient/patient-response';
import { BlobResourceResponse } from 'src/models/shared/blob-resource-response';
import { Constants } from 'src/models/shared/constants';

@Injectable({
  providedIn: 'root'
})
export class PatientService {
  private readonly patientsEndpointBase: string = `${Constants.API_ENDPOINT}/patients`;

  constructor(
    private httpClient: HttpClient
  ) { }

  create(request: CreatePatientRequest): Observable<string> {
    return this.httpClient.post<string>(this.patientsEndpointBase, request);
  }

  getAll(): Observable<PatientResponse[]> {
    return this.httpClient.get<PatientResponse[]>(this.patientsEndpointBase);
  }

  delete(id) :Observable<any> {
    return this.httpClient.delete(`${this.patientsEndpointBase}/${id}`);
  }

  getPatientImage(id): Observable<BlobResourceResponse> {
    return this.httpClient.get<BlobResourceResponse>(`${this.patientsEndpointBase}/${id}/image`)
  }

  verifyPatient(id): Observable<any> {
    return this.httpClient.post(`${this.patientsEndpointBase}/${id}/verify`, {});
  }
}
