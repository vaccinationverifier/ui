export interface CreateVaccineRequest {
  illnessName: string;
  vaccineName: string;
  type: string;
  description: string;
  vendor: string;
  valence: string;
  license: string;
  sideEffects: string;
}
