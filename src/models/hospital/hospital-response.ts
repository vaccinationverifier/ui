import { Address } from "../shared/address";

export interface HospitalResponse {
  id: string;
  name: string;
  address: Address;
}
