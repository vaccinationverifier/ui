import { Address } from "../shared/address";

export interface CreateHospitalRequest {
  name: string;
  address: Address
}
