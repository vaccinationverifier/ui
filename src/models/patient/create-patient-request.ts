export interface CreatePatientRequest {
  dateOfBirth: string;
  street: string;
  city: string;
  state: string;
  country: string;
  zipCode: string;
}
