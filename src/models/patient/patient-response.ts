import { Address } from "../shared/address";

export interface PatientResponse {
  id: string;
  name: string;
  email: string;
  dateOfBirth: string;
  address: Address
}
