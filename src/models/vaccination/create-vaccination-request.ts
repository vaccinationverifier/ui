export interface CreateVaccinationRequest {
  vaccineId: string;
  patientId: string;
}
