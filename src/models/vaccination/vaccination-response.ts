import { Address } from "../shared/address";

export interface VaccinationResponse {
  id: string;
  illnessName: string;
  vaccineName: string
  hospitalName: string;
  hospitalAddress: Address;
  vaccinationStatus: string;
  createdAt: string;
}
