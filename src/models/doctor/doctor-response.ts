export interface DoctorResponse {
  id: string;
  name: string;
  email: string;
  specialty: string;
  hospital: string;
  verification: string
  professionalSummary: string;
  experience: number
}
