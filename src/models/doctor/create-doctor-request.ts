export interface CreateDoctorRequest {
  specialty: string;
  experience: number;
  professionalSummary: string;
  hospitalId: string;
}
