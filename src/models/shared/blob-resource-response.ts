export interface BlobResourceResponse {
  url: string;
}
