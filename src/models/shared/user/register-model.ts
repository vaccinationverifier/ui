export interface RegisterModel {
  login: string;
  email: string;
  password: string;
  firstName: string;
  lastName: string;
}
