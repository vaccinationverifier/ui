export interface BanUserRequest {
  banPeriod: string;
  until?: string;
}
