export interface UserInfo {
  id: string;
  firstName: string;
  lastName: string;
  login: string;
  email: string;
  bannedUntil: string;
}
