import { AuthInterceptor } from '../interseptors/auth-interceptor';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { MatSelectModule } from '@angular/material/select';
import { CdkTreeModule } from '@angular/cdk/tree';
import { MatIconModule } from '@angular/material/icon';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatDialogModule } from '@angular/material/dialog';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatMenuModule } from '@angular/material/menu';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatCardModule } from '@angular/material/card';
import { SignInComponent } from './auth/sign-in/sign-in.component';
import { SignUpComponent } from './auth/sign-up/sign-up.component';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatListModule } from '@angular/material/list';
import { MatBadgeModule } from '@angular/material/badge';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatTableModule } from '@angular/material/table';
import { FileUploadComponent } from './shared/file-upload/file-upload.component';
import { CreatePatientComponent } from './patients/create-patient/create-patient.component';
import { CreateDoctorComponent } from './doctors/create-doctor/create-doctor.component';
import { MatNativeDateModule } from '@angular/material/core';
import { CreateHospitalComponent } from './hospitals/create-hospital/create-hospital.component';
import { CreateVaccineComponent } from './vaccines/create-vaccine/create-vaccine.component';
import { VaccineListComponent } from './vaccines/vaccine-list/vaccine-list.component';
import { HospitalListComponent } from './hospitals/hospital-list/hospital-list.component';
import { CreateVaccinationComponent } from './vaccinations/create-vaccination/create-vaccination.component';
import { UserListComponent } from './users/user-list/user-list.component';
import { VaccinationListComponent } from './vaccinations/vaccination-list/vaccination-list.component';
import { PatientListComponent } from './patients/patient-list/patient-list.component';
import { DoctorListComponent } from './doctors/doctor-list/doctor-list.component';
import { BanComponent } from './users/ban/ban.component';

@NgModule({
  declarations: [
    AppComponent,
    SignInComponent,
    SignUpComponent,
    FileUploadComponent,
    CreatePatientComponent,
    CreateDoctorComponent,
    CreateHospitalComponent,
    CreateVaccineComponent,
    VaccineListComponent,
    HospitalListComponent,
    CreateVaccinationComponent,
    UserListComponent,
    VaccinationListComponent,
    PatientListComponent,
    DoctorListComponent,
    BanComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatInputModule,
    MatButtonModule,
    MatSelectModule,
    CdkTreeModule,
    MatIconModule,
    MatExpansionModule,
    MatDialogModule,
    MatSnackBarModule,
    MatMenuModule,
    MatToolbarModule,
    MatCardModule,
    MatSidenavModule,
    MatListModule,
    MatBadgeModule,
    MatTooltipModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatTableModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})

export class AppModule { }
