import { Component } from '@angular/core';
import { AuthorizationService } from 'src/services/authorization.service';
import { LocalizationService } from 'src/services/localization.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(
    public authService: AuthorizationService,
    private localizationService: LocalizationService,
  ) { }

  getLocalizationPath(locale: string) {
    return this.localizationService.getLocalizationPath(locale);
  }
}
