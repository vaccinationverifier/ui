import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators } from '@angular/forms';
import { SelectValue } from 'src/models/shared/select-value';
import { BanUserRequest } from 'src/models/shared/user/ban-user-request';
import { UserService } from 'src/services/user.service';

@Component({
  selector: 'app-ban',
  templateUrl: './ban.component.html',
  styleUrls: ['./ban.component.css']
})
export class BanComponent implements OnInit {

  constructor(
    private userService: UserService,
  ) { }

  banPeriods: SelectValue[];

  ngOnInit(): void {
    this.banPeriods = this.userService.getBanPeriods().map(p => {
      return { value: p, viewValue: p };
    })
  }

  banForm = new FormGroup({
    banPeriod: new FormControl('', Validators.required),
    until: new FormControl(''),
  });

  get banPeriod() { return this.banForm.get('banPeriod'); }
  get until() { return this.banForm.get('until'); }


  fetchFormData(): BanUserRequest {
    if (this.banForm.invalid) {
      return;
    }

    if(this.banPeriod.value == this.userService.getCustomBanPeriod() && !this.until.value) {
      return;
    }

    return this.banForm.value;
  }
}
