import {Component, OnInit, ViewChild} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {UserInfo} from 'src/models/shared/user/user-info';
import {UserService} from 'src/services/user.service';
import {BanComponent} from '../ban/ban.component';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {

  constructor(
    private userService: UserService,
    private dialog: MatDialog,
  ) {
  }

  displayedColumns: string[] = ['action', 'firstName', 'lastName', 'email', 'login'];
  dataSource: UserInfo[];
  backupUrl: string;

  @ViewChild(BanComponent)
  private banComponent: BanComponent;

  ngOnInit(): void {
    this.userService.getAll().subscribe(users => this.dataSource = users);
  }

  banUser(element: UserInfo) {
    const dialogRef = this.dialog.open(BanComponent, {
      width: '450px'
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log("dialog result: "+ result);
      if (!result) {
        return;
      }

      this.userService.banUser(element.id, result).subscribe(_ => this.userService.getAll()
        .subscribe(users => this.dataSource = users));
    });
  }

  downloadBackup() {
    this.userService.downloadBackup().subscribe(backup => {
      const downloadURL = window.URL.createObjectURL(backup);
      const link = document.createElement('a');
      link.href = downloadURL;
      link.download = "backup.bak";
      link.click();
    })
  }
}
