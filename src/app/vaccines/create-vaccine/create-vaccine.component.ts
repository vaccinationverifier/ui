import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {CreateVaccineRequest} from 'src/models/vaccine/create-vaccine-request';
import {VaccineService} from 'src/services/vaccine.service';

@Component({
  selector: 'app-create-vaccine',
  templateUrl: './create-vaccine.component.html',
  styleUrls: ['./create-vaccine.component.css']
})
export class CreateVaccineComponent {

  constructor(
    private vaccineService: VaccineService,
    private router: Router,
  ) {
  }

  createVaccineForm: FormGroup = new FormGroup({
    illnessName: new FormControl('', Validators.required),
    vaccineName: new FormControl('', Validators.required),
    type: new FormControl('', Validators.required),
    description: new FormControl('', Validators.required),
    vendor: new FormControl('', Validators.required),
    valence: new FormControl('', Validators.required),
    license: new FormControl('', Validators.required),
    sideEffects: new FormControl('', Validators.required),
  });

  get illnessName() {
    return this.createVaccineForm.get('illnessName');
  }

  get vaccineName() {
    return this.createVaccineForm.get('vaccineName');
  }

  get type() {
    return this.createVaccineForm.get('type');
  }

  get description() {
    return this.createVaccineForm.get('description');
  }

  get vendor() {
    return this.createVaccineForm.get('vendor');
  }

  get valence() {
    return this.createVaccineForm.get('valence');
  }

  get license() {
    return this.createVaccineForm.get('license');
  }

  get sideEffects() {
    return this.createVaccineForm.get('sideEffects');
  }

  createVaccine() {
    if (this.createVaccineForm.invalid) {
      return;
    }

    const vaccineData = this.createVaccineForm.value;

    this.vaccineService.create(vaccineData).subscribe(
      async (id: any) => await this.router.navigate(['vaccines'])
    );
  }
}
