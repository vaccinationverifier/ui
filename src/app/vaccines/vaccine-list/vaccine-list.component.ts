import {Component, OnInit} from '@angular/core';
import {VaccineResponse} from 'src/models/vaccine/vaccine-response';
import {VaccineService} from 'src/services/vaccine.service';

@Component({
  selector: 'app-vaccine-list',
  templateUrl: './vaccine-list.component.html',
  styleUrls: ['./vaccine-list.component.css']
})
export class VaccineListComponent implements OnInit {

  constructor(
    private vaccineService: VaccineService,
  ) {
  }

  displayedColumns: string[] = ['action', 'illnessName', 'vaccineName', 'vendor', 'valence'];
  dataSource: VaccineResponse[];

  ngOnInit(): void {
    this.vaccineService.getAll().subscribe(result => this.dataSource = result);
  }

  deleteVaccine(vaccine) {
    this.vaccineService.delete(vaccine.id)
      .subscribe(res => this.vaccineService.getAll()
        .subscribe(vaccines => this.dataSource = vaccines));
  }
}
