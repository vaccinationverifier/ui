import {AuthorizationService} from './../../../services/authorization.service';
import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {CustomValidators} from 'src/models/shared/custom-validators';
import {SelectValue} from 'src/models/shared/select-value';
import {CreatePatientComponent} from 'src/app/patients/create-patient/create-patient.component';
import {CreateDoctorComponent} from 'src/app/doctors/create-doctor/create-doctor.component';
import {MatDialog} from '@angular/material/dialog';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {

  roles: SelectValue[];
  roleToComponentMap;

  signUpForm: FormGroup = new FormGroup({
    firstName: new FormControl('', Validators.required),
    lastName: new FormControl('', Validators.required),
    login: new FormControl('', Validators.required),
    email: new FormControl('', Validators.compose([Validators.required, Validators.email])),
    password: new FormControl('', Validators.compose([
        Validators.required,
        CustomValidators.patternValidator(/\d/, {
          hasNumber: true
        }),
        CustomValidators.patternValidator(/[A-Z]/, {
          hasCapitalCase: true
        }),
        CustomValidators.patternValidator(/[a-z]/, {
          hasSmallCase: true
        }),
        // CustomValidators.patternValidator(
        //   /[ !@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/,
        //   {
        //     hasSpecialCharacters: true
        //   }
        // ),
        Validators.minLength(8)
      ])
    ),
    role: new FormControl('', Validators.required)
  });

  get firstName() {
    return this.signUpForm.get('firstName');
  }

  get lastName() {
    return this.signUpForm.get('lastName');
  }

  get login() {
    return this.signUpForm.get('lastName');
  }

  get email() {
    return this.signUpForm.get('email');
  }

  get password() {
    return this.signUpForm.get('password');
  }

  get role() {
    return this.signUpForm.get('role');
  }

  error: string;

  constructor(
    private authService: AuthorizationService,
    private router: Router
  ) {
  }

  ngOnInit(): void {
    this.roles = [
      {value: "Patient", viewValue: "Patient"},
      {value: "Doctor", viewValue: "Doctor"}
    ];

    this.roleToComponentMap = {
      "Patient": "patients/new",
      "Doctor": "doctors/new"
    };
  }

  registerUser() {
    if (!this.signUpForm.valid) {
      return;
    }

    console.log(this.signUpForm.value);
    this.authService.signUp(this.signUpForm.value).subscribe(
      async (res: any) => {
        this.authService.setToken(res.token);
        const endpoint = this.roleToComponentMap[this.role.value]

        await this.router.navigate([endpoint]);
      },
      error => this.error = error.message
    )
  }
}
