import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthorizationService } from 'src/services/authorization.service';
import { CustomValidators } from 'src/models/shared/custom-validators';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css']
})
export class SignInComponent {
  signInForm: FormGroup = new FormGroup({
    login: new FormControl('', Validators.compose([Validators.required])),
    password: new FormControl('', Validators.compose([
      Validators.required,
      CustomValidators.patternValidator(/\d/, {
        hasNumber: true
      }),
      CustomValidators.patternValidator(/[A-Z]/, {
        hasCapitalCase: true
      }),
      CustomValidators.patternValidator(/[a-z]/, {
        hasSmallCase: true
      }),
      // CustomValidators.patternValidator(
      //   /[ !@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/,
      //   {
      //     hasSpecialCharacters: true
      //   }
      // ),
      Validators.minLength(8)
    ])
  )});

  get login() { return this.signInForm.get('login'); }
  get password() { return this.signInForm.get('password'); }

  error: string;

  constructor(
    public authService: AuthorizationService,
    public router: Router
  ) { }

  loginUser() {
    if(!this.signInForm.valid){
      return;
    }

    this.authService.signIn(this.signInForm.value).subscribe(
      (res: any) => {
        this.authService.setTokenAndRedirectToHomePage(res.token, "vaccines");
      },
      (error) => {
        this.error = error.message;
      }
    );
  }
}
