import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {AuthGuard} from 'src/guards/auth.guard';
import {SignUpComponent} from './auth/sign-up/sign-up.component';
import {SignInComponent} from './auth/sign-in/sign-in.component';
import {CreateDoctorComponent} from './doctors/create-doctor/create-doctor.component';
import {CreatePatientComponent} from './patients/create-patient/create-patient.component';
import {CreateVaccineComponent} from './vaccines/create-vaccine/create-vaccine.component';
import {CreateHospitalComponent} from './hospitals/create-hospital/create-hospital.component';
import {VaccineListComponent} from './vaccines/vaccine-list/vaccine-list.component';
import { HospitalListComponent } from './hospitals/hospital-list/hospital-list.component';
import { CreateVaccinationComponent } from './vaccinations/create-vaccination/create-vaccination.component';
import { VaccinationListComponent } from './vaccinations/vaccination-list/vaccination-list.component';
import { DoctorListComponent } from './doctors/doctor-list/doctor-list.component';
import { PatientListComponent } from './patients/patient-list/patient-list.component';
import { UserListComponent } from './users/user-list/user-list.component';

const routes: Routes = [
  {path: '', redirectTo: 'log-in', pathMatch: 'full'},
  // { path: 'wallets/my', component: WalletMyComponent, canActivate: [AuthGuard]},
  {path: 'log-in', component: SignInComponent},
  {path: 'sign-up', component: SignUpComponent},
  {path: 'doctors/new', component: CreateDoctorComponent},
  {path: 'patients/new', component: CreatePatientComponent},
  {path: 'vaccines/new', component: CreateVaccineComponent},
  {path: 'hospitals/new', component: CreateHospitalComponent},
  {path: 'vaccinations/new', component: CreateVaccinationComponent},
  {path: 'vaccines', component: VaccineListComponent},
  {path: 'hospitals', component: HospitalListComponent},
  {path: 'vaccinations', component: VaccinationListComponent},
  {path: 'patients', component: PatientListComponent},
  {path: 'doctors', component: DoctorListComponent},
  {path: 'users', component: UserListComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
