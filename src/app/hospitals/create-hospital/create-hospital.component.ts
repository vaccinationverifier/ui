import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CreateHospitalRequest } from 'src/models/hospital/create-hospital-request';
import { Address } from 'src/models/shared/address';
import { HospitalService } from 'src/services/hospital.service';

@Component({
  selector: 'app-create-hospital',
  templateUrl: './create-hospital.component.html',
  styleUrls: ['./create-hospital.component.css']
})
export class CreateHospitalComponent implements OnInit {

  constructor(
    private hospitalService: HospitalService,
    private router: Router,
  ) { }

  ngOnInit(): void {
  }

  createHospitalForm: FormGroup = new FormGroup({
    name: new FormControl('', Validators.required),
    street: new FormControl('', Validators.required),
    city: new FormControl('', Validators.required),
    state: new FormControl('', Validators.required),
    country: new FormControl('', Validators.required),
    zipCode: new FormControl('', Validators.required),
  });

  get name() {
    return this.createHospitalForm.get('name');
  }

  get street() {
    return this.createHospitalForm.get('street');
  }

  get city() {
    return this.createHospitalForm.get('city');
  }

  get state() {
    return this.createHospitalForm.get('state');
  }

  get country() {
    return this.createHospitalForm.get('country');
  }

  get zipCode() {
    return this.createHospitalForm.get('zipCode');
  }

  createHospital() {
    if (this.createHospitalForm.invalid) {
      return;
    }

    const address: Address = {
      city: this.city.value,
      state: this.state.value,
      zipCode: this.zipCode.value,
      street: this.street.value,
      country: this.country.value
    };

    const request: CreateHospitalRequest = {
      name: this.name.value,
      address: address
    };

    this.hospitalService.create(request).subscribe(async id => {
      await this.router.navigate(['hospitals']);
    })
  }
}
