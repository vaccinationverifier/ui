import {Component, OnInit} from '@angular/core';
import {HospitalResponse} from 'src/models/hospital/hospital-response';
import {HospitalService} from 'src/services/hospital.service';

@Component({
  selector: 'app-hospital-list',
  templateUrl: './hospital-list.component.html',
  styleUrls: ['./hospital-list.component.css']
})
export class HospitalListComponent implements OnInit {

  constructor(
    private hospitalService: HospitalService,
  ) {
  }

  ngOnInit(): void {
    this.hospitalService.getByAddress({}).subscribe(hospitals => this.dataSource = hospitals);
  }

  displayedColumns: string[] = ['action', 'name', 'street', 'city', 'country'];
  dataSource: HospitalResponse[];

  deleteHospital(hospital: HospitalResponse) {
    this.hospitalService.delete(hospital.id).subscribe(_ => this.hospitalService.getByAddress({})
      .subscribe(res => this.dataSource = res));
  }
}
