import { Component, OnInit } from '@angular/core';
import { DoctorResponse } from 'src/models/doctor/doctor-response';
import { DoctorService } from 'src/services/doctor.service';
import {animate, state, style, transition, trigger} from '@angular/animations';

@Component({
  selector: 'app-doctor-list',
  templateUrl: './doctor-list.component.html',
  styleUrls: ['./doctor-list.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class DoctorListComponent implements OnInit {

  constructor(
    private doctorService: DoctorService
  ) { }

  columnsToDisplay: string[] = ['name', 'email', 'specialty', 'hospital', 'verification'];
  dataSource: DoctorResponse[];
  expandedElement: DoctorResponse | null;
  doctorImage: string | null;

  ngOnInit(): void {
    this.doctorService.getAll().subscribe(patients => this.dataSource = patients);
  }

  expandElement(element) {
    this.expandedElement = this.expandedElement === element ? null : element;
    this.doctorService.getDoctorImage(element.id)
      .subscribe(res => this.doctorImage = res.url);
  }

  verifyDoctor(element) {
    this.doctorService.verifyDoctor(element.id).subscribe(_ =>
      this.doctorService.getAll().subscribe(res => this.dataSource = res));
  }
}
