import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import { Router } from '@angular/router';
import {FileUploadComponent} from 'src/app/shared/file-upload/file-upload.component';
import {SelectValue} from 'src/models/shared/select-value';
import {DoctorService} from 'src/services/doctor.service';
import {HospitalService} from 'src/services/hospital.service';

@Component({
  selector: 'app-create-doctor',
  templateUrl: './create-doctor.component.html',
  styleUrls: ['./create-doctor.component.css']
})
export class CreateDoctorComponent implements OnInit {
  hospitals: SelectValue[];

  error: string;

  createDoctorForm: FormGroup = new FormGroup({
    specialty: new FormControl('', Validators.required),
    experience: new FormControl('', Validators.required),
    professionalSummary: new FormControl('', Validators.required),
    hospitalId: new FormControl('', Validators.required)
  });

  get specialty() {
    return this.createDoctorForm.get('specialty');
  }

  get experience() {
    return this.createDoctorForm.get('experience');
  }

  get professionalSummary() {
    return this.createDoctorForm.get('professionalSummary');
  }

  get hospitalId() {
    return this.createDoctorForm.get('hospitalId');
  }

  constructor(
    private doctorService: DoctorService,
    private hospitalSerive: HospitalService,
    private router: Router
  ) {
  }

  @ViewChild(FileUploadComponent)
  private fileUpload: FileUploadComponent;

  ngOnInit(): void {
    this.hospitalSerive.getByAddress({}).subscribe(
      hospitals => {
        this.hospitals = hospitals.map(h => {
          const viewValue = `${h.name}. City: ${h.address.city}`;
          return {value: h.id, viewValue: viewValue}
        });
      })
  }

  createDoctor() {
    if (this.createDoctorForm.invalid) {
      return;
    }

    if (!this.fileUpload.isFileSelected()){
      this.error = "Verification document is not specified"
      return;
    }

    this.error = '';
    const doctorData = this.createDoctorForm.value;

    try {
      this.doctorService.create(doctorData).subscribe(
        async (id: any) => {
          this.fileUpload.upload(`doctors/upload`);
          await this.router.navigate(["doctors"])
        }
      )
    } catch (e) {
      console.log(e);
    }
  }
}
