import {Component, OnInit} from '@angular/core';
import {PatientResponse} from 'src/models/patient/patient-response';
import {PatientService} from 'src/services/patient.service';
import {animate, state, style, transition, trigger} from '@angular/animations';

@Component({
  selector: 'app-patient-list',
  templateUrl: './patient-list.component.html',
  styleUrls: ['./patient-list.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class PatientListComponent implements OnInit {

  constructor(
    private patientService: PatientService,
  ) {
  }

  columnsToDisplay: string[] = ['name', 'email', 'dateOfBirth', 'city', 'country', 'verification'];
  dataSource: PatientResponse[];
  expandedElement: PatientResponse | null;
  patientImage: string | null;

  ngOnInit(): void {
    this.patientService.getAll().subscribe(patients => this.dataSource = patients);
  }

  expandElement(element) {
    this.expandedElement = this.expandedElement === element ? null : element;
    this.patientService.getPatientImage(element.id)
      .subscribe(res => this.patientImage = res.url);
  }

  verifyPatient(element) {
    this.patientService.verifyPatient(element.id).subscribe(_ =>
      this.patientService.getAll().subscribe(res => this.dataSource = res));
  }
}
