import {Component, OnInit, ViewChild} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import { Router } from '@angular/router';
import {FileUploadComponent} from 'src/app/shared/file-upload/file-upload.component';
import {PatientService} from 'src/services/patient.service';

@Component({
  selector: 'app-create-patient',
  templateUrl: './create-patient.component.html',
  styleUrls: ['./create-patient.component.css']
})
export class CreatePatientComponent {
  error: string;

  createPatientForm: FormGroup = new FormGroup({
    dateOfBirth: new FormControl('', Validators.required),
    street: new FormControl('', Validators.required),
    city: new FormControl('', Validators.required),
    state: new FormControl('', Validators.required),
    country: new FormControl('', Validators.required),
    zipCode: new FormControl('', Validators.required),
  });

  get dateOfBirth() {
    return this.createPatientForm.get('dateOfBirth');
  }

  get street() {
    return this.createPatientForm.get('street');
  }

  get city() {
    return this.createPatientForm.get('city');
  }

  get state() {
    return this.createPatientForm.get('state');
  }

  get country() {
    return this.createPatientForm.get('country');
  }

  get zipCode() {
    return this.createPatientForm.get('zipCode');
  }

  constructor(
    private patientService: PatientService,
    private router: Router
  ) {
  }

  @ViewChild(FileUploadComponent)
  private fileUpload: FileUploadComponent;

  createPatient() {
    if (this.createPatientForm.invalid) {
      return;
    }

    if (!this.fileUpload.isFileSelected()){
      this.error = "Verification document is not specified"
      return;
    }

    this.error = '';
    const patientData = this.createPatientForm.value;

    try {
      this.patientService.create(patientData).subscribe(
        async (id: any) => {
          this.fileUpload.upload(`patients/upload`);
          await this.router.navigate(['patients']);
        }
      )
    } catch (e) {
      console.log(e);
    }
  }
}
