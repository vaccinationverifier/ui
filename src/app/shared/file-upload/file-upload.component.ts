import {HttpClient, HttpEventType, HttpHeaders} from '@angular/common/http';
import {Component, Input, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {finalize} from "rxjs/operators"
import {Constants} from 'src/models/shared/constants';

@Component({
  selector: 'app-file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.css']
})
export class FileUploadComponent {

  @Input()
  requiredFileType: string;

  file: File;
  fileName = '';
  uploadProgress: number;
  uploadSub: Subscription;

  constructor(private http: HttpClient) {
  }

  upload(endpoint: string) {
    if (!this.file) {
      throw "File is not selected";
    }

    const formData = new FormData();
    formData.append("file", this.file, this.fileName);
    let headers = new HttpHeaders();
    headers.append('Content-Type', 'multipart/form-data');
    headers.append('Accept', 'application/json');

    const upload$ = this.http.post(`${Constants.API_ENDPOINT}/${endpoint}`, formData, {
      headers: headers,
      reportProgress: true,
      observe: 'events',
    })
      .pipe(
        finalize(() => this.reset())
      );

    this.uploadSub = upload$.subscribe(event => {
      if (event.type == HttpEventType.UploadProgress) {
        this.uploadProgress = Math.round(100 * (event.loaded / event.total));
      }
    })
  }

  onFileSelected(event) {
    this.file = event.target.files[0];

    if (this.file) {
      this.fileName = this.file.name;
    }
  }

  cancelUpload() {
    this.uploadSub.unsubscribe();
    this.reset();
  }

  reset() {
    this.uploadProgress = null;
    this.uploadSub = null;
  }

  isFileSelected() {
    return this.file ? true : false;
  }
}
