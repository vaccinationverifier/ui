import {Component, OnInit} from '@angular/core';
import {VaccinationResponse} from 'src/models/vaccination/vaccination-response';
import {VaccinationService} from 'src/services/vaccination.service';
import {animate, state, style, transition, trigger} from '@angular/animations';

@Component({
  selector: 'app-vaccination-list',
  templateUrl: './vaccination-list.component.html',
  styleUrls: ['./vaccination-list.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class VaccinationListComponent implements OnInit {

  constructor(
    private vaccinationService: VaccinationService
  ) {
  }

  displayedColumns: string[] = [
    'illnessName',
    'vaccineName',
    'hospitalName',
    'doctorName',
    'patientName',
    'vaccinationStatus',
    'createdAt'
  ];
  dataSource: VaccinationResponse[];
  expandedElement: VaccinationResponse | null;

  video: string | null;
  currentPlayingVideo: HTMLVideoElement;

  ngOnInit(): void {
    this.vaccinationService.my().subscribe(result => this.dataSource = result);
  }

  expandElement(element) {
    this.expandedElement = this.expandedElement === element ? null : element;
    this.vaccinationService.getVaccinationVideo(element.id)
      .subscribe(res => this.video = res.url);
  }


  onPlayingVideo(event) {
    event.preventDefault();
    if (this.currentPlayingVideo === undefined) {
      this.currentPlayingVideo = event.target;
      this.currentPlayingVideo.play();
    } else {
      if (event.target !== this.currentPlayingVideo) {
        this.currentPlayingVideo.pause();
        this.currentPlayingVideo = event.target;
        this.currentPlayingVideo.play();
      }
    }
  }
}
