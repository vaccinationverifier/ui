import { Component, OnInit, ViewChild } from '@angular/core';
import {FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { FileUploadComponent } from 'src/app/shared/file-upload/file-upload.component';
import { SelectValue } from 'src/models/shared/select-value';
import { PatientService } from 'src/services/patient.service';
import { VaccinationService } from 'src/services/vaccination.service';
import { VaccineService } from 'src/services/vaccine.service';

@Component({
  selector: 'app-create-vaccination',
  templateUrl: './create-vaccination.component.html',
  styleUrls: ['./create-vaccination.component.css']
})
export class CreateVaccinationComponent implements OnInit {

  error: string;

  createVaccinationForm: FormGroup = new FormGroup({
    patientId: new FormControl('', Validators.required),
    vaccineId: new FormControl('', Validators.required),
  });

  @ViewChild(FileUploadComponent)
  private fileUpload: FileUploadComponent;

  constructor(
    private patientService: PatientService,
    private vaccineService: VaccineService,
    private vaccinationService: VaccinationService,
    private router: Router,
  ) { }

  patientValues: SelectValue[];
  vaccineValues: SelectValue[];

  ngOnInit(): void {
    this.patientService.getAll().subscribe(patients => {
      console.log(patients);
      this.patientValues = patients.map(p => {
        return { value: p.id, viewValue: `Name: ${p.name}. Email: ${p.email}` };
      })
    });

    this.vaccineService.getAll().subscribe(vaccines => this.vaccineValues = vaccines.map(v => {
      return { value: v.id, viewValue: v.vaccineName }
    }))
  }

  createVaccination() {
    if (this.createVaccinationForm.invalid) {
      return;
    }

    if (!this.fileUpload.isFileSelected()){
      this.error = "Verification document is not specified"
      return;
    }

    this.vaccinationService.create(this.createVaccinationForm.value).subscribe(async resp => {
      this.fileUpload.upload(`vaccinations/${resp.id}/upload`);
      await  this.router.navigate(["vaccinations"])
    })
  }
}
